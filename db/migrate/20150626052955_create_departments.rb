class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.belongs_to :company, index: true, foreign_key: true
      t.string :dept_name
      t.string :dept_description

      t.timestamps null: false
    end
  end
end
