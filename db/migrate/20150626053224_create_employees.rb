class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.belongs_to :department, index: true, foreign_key: true
      t.string :emp_name
      t.string :emp_address
      t.integer :age
      t.string :working_location
      t.integer :salary

      t.timestamps null: false
    end
  end
end
