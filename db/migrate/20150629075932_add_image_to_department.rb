class AddImageToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :image, :string
  end
end
