class CompanyController < ApplicationController
  def create
      Company.create params.require(:company).permit(:company_name, :location, :company_description)
      redirect_to :action => 'show'
  end


  def show
  	@companies=Company.all
  end	

  def edit
  	@company = Company.find(params[:id])
  	
  end

  def update

    @company = Company.find(params[:company][:id])
      if @company.update_attributes(params.require(:company).permit(:company_name, :location ,:company_description))
         redirect_to :action => 'show'
      else
         render :action => 'edit'
      end

  end

  def delete
    
  	  Company.find(params[:id]).destroy
    	redirect_to :action => 'show'
   end

  def new
   		@companies=Company.new
  end

end
