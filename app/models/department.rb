class Department < ActiveRecord::Base
  belongs_to :company
  has_many :employees
  
  mount_uploader :image, ImageUploader
end
