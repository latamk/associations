class ListingController < ApplicationController
		
	def show
		@record = Record.all
	end

	def create 
	end	

	def new
	end

	def results
    	agent = Mechanize.new
    	page = agent.get('http://jobsearch.naukri.com/client-server-jobs?xt=catsrch&qf[]=24.02')
    	records = page.search ".content"
        @list_arr = []
        records.each do |record|
        	heading = record.search('.desig').text
            company_name = record.search('.org').text
			experience=record.search('.exp').text
			location=record.search('.span').text
			job_skills=record.search('.label').text
			job_description=record.search('.desc').text
			posted_by=record.search('.rec_details').text

			list = Record.create(:heading => heading ,:companyname => company_name , :experience_required =>experience , :location =>location , :job_skills => job_skills ,:job_description =>job_description ,:posted_by =>posted_by)
			@list_arr << list
		end
	end
end