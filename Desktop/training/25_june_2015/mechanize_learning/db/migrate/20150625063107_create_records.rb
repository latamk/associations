class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string  :companyname
      t.string  :job_description
      t.string  :job_skills
      t.string  :location
      t.string  :heading
      t.string  :posted_by
      t.integer :experience_required
      t.timestamps null: false
    end
  end
end
