class EmployeeController < ApplicationController
	
  def create
    Employee.create params.require(:employee).permit(:emp_name, :emp_address, :age ,:working_location ,:salary)
    redirect_to :action => 'show'
  end

	def show
  		@employees=Employee.all
  end	

  def edit
      @employee = Employee.find(params[:id])
      
  end

  def update

    @employee = Employee.find(params[:employee][:id])
    @employee.update_attributes(params.require(:employee).permit(:emp_name,:emp_address,:age ,:working_location ,:salary, :department_id))
         

  end  

  def delete
   		Employee.find(params[:id]).destroy
      	redirect_to :action => 'show'
  end

  def new
   		@employee=Employee.new
  end	
end
