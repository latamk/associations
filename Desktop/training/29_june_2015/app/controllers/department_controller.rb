class DepartmentController < ApplicationController
  def create
    Department.create params.require(:department).permit(:dept_name, :dept_description,:image)
    respond_to do |format|
    format.html {redirect_to :action => 'show'}
    format.js
    end
  end

  def show
   	@departments=Department.all
  end

  def edit
  	@department = Department.find(params[:id])
  end

  def update
      @department = Department.find(params[:department][:id])
      @department.update_attributes(params.require(:department).permit(:dept_name ,:dept_description,:company_id))

  end  

  def delete
   	  Department.find(params[:id]).destroy
      respond_to do |format|
      format.html {redirect_to :action => 'show'}
      format.js
      end
  end

  def new
   	   @department=Department.new
  end	
end
