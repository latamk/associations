Rails.application.routes.draw do
  root "home#index"
  get 'employee/show',as: :emp_show
  get 'department/show',as: :dept_show
  get  'company/show',as: :comp_show
  get  'company/edit',as: :change_edit
  patch 'company/update',as: :update_company
  get  'department/edit',as: :dept_edit
  patch 'department/update',as: :update_department
  get  'employee/edit',as: :emp_edit
  patch  'employee/update',as: :update_employee
  get 'employee/delete',as: :delete_emp
  get  'department/delete',as: :delete_dept
  get  'company/delete',as: :delete_comp
  get  'company/new',as: :new_comp 
  get  'department/new',as: :new_dept
  post 'company/create', as: :create_company
  post 'department/create',as: :create_department
  get  'employee/new',as: :new_emp
  post  'employee/create',as: :create_employee

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
